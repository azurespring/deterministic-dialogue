<?php

namespace AzureSpring\DeterministicDialogue;

interface BrokerInterface
{
    /**
     * send message
     *
     * @param string $message
     * @return $this
     */
    public function send( /* string */ $message );
}
