<?php

namespace AzureSpring\DeterministicDialogue\Question;

interface QuestionInterface
{
    /**
     * get question
     *
     * @return string
     */
    public function getQuestion();

    /**
     * filter answer
     *
     * @param string $answer
     *
     * @return string|false
     */
    public function filter( /* string */ $answer );
}
