<?php

namespace AzureSpring\DeterministicDialogue\Question;

class Question implements QuestionInterface
{
    private $question;


    /**
     * @param string $question
     */
    public function __construct( $question )
    {
        $this->question = $question;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * {@inheritDoc}
     */
    public function filter( /* string */ $answer )
    {
        return $answer;
    }
}
