<?php
namespace AzureSpring\DeterministicDialogue\Question;

class ChoiceQuestion implements QuestionInterface
{
    private $options;


    /**
     * @param string[] $options
     */
    public function __construct( array $options )
    {
        $this->options = [];

        $i = 0;
        foreach ( $options as $k => $v )
            $this->options[ is_int( $k ) ? ++$i : $k ] = $v;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuestion()
    {
        return implode("\n", array_map(
            function ( $k, $v ) {
                if (preg_match( "/\\W/", $k ))
                    return $v;

                return sprintf( '%s. %s', $k, $v );
            },
            array_keys( $this->options ),
            array_values( $this->options )
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function filter( /* string */ $answer )
    {
        if (preg_match( "/\\W/", $answer ) ||
            !array_key_exists( $answer, $this->options ))
            return false;

        return $answer;
    }
}
