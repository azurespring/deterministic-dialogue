<?php

namespace AzureSpring\DeterministicDialogue;

abstract class AbstractDialogue implements DialogueInterface
{
    public function ask( Question\QuestionInterface $question )
    {
        try {
            // try to recv without ask first
            return $question->filter( $this->recv() );
        }
        catch ( Exception\ContinueException $e ) {
            // ask if no answer available
            $this->send( $question->getQuestion() );

            return $question->filter( $this->recv() );
        }
    }
}
