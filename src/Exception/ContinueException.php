<?php

namespace AzureSpring\DeterministicDialogue\Exception;

class ContinueException extends \Exception implements ExceptionInterface
{
}
