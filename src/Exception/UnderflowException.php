<?php

namespace AzureSpring\DeterministicDialogue\Exception;

class UnderflowException extends \Exception implements ExceptionInterface
{
}
