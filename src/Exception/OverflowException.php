<?php

namespace AzureSpring\DeterministicDialogue\Exception;

class OverflowException extends \Exception implements ExceptionInterface
{
}
