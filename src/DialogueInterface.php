<?php

namespace AzureSpring\DeterministicDialogue;

interface DialogueInterface
{
    /**
     * reset dialogue
     *
     * @return $this
     */
    public function reset();

    /**
     * send (text) message
     *
     * @param string $message
     * @return $this
     */
    public function send( /* string */ $message );

    /**
     * receive (text) message
     *
     * @return string
     * @throws Exception\ContinueException
     * @throws Exception\UnderflowException
     */
    public function recv();

    /**
     * ask question, and receive the answer
     *
     * @param Question\QuestionInterface $question
     * @return string
     * @throws Exception\ContinueException
     * @throws Exception\UnderflowException
     */
    public function ask( Question\QuestionInterface $question );

    /**
     * executes a function in a transaction (non-deterministic)
     *
     * @param callable $func
     * @return mixed
     * @throws Exception\ContinueException
     * @throws Exception\UnderflowException
     */
    public function transactional( $func );
}
