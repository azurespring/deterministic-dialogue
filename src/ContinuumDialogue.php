<?php

namespace AzureSpring\DeterministicDialogue;

class ContinuumDialogue extends AbstractDialogue
{
    private $broker;

    private $actionStack;

    private $actionPtr;

    private $messagePtr;


    /**
     * @param callable $send
     * @param string[][] $actionStack
     */
    public function __construct( BrokerInterface $broker, array $actionStack = null )
    {
        $this->broker       = $broker;
        $this->actionStack  = $actionStack ?: [[]];
        $this->actionPtr    = count( $this->actionStack ) - 1;
        $this->messagePtr   = 0;
    }

    /**
     * {@inheritDoc}
     */
    public function reset()
    {
        $this->actionStack  = [[]];
        $this->actionPtr    = count( $this->actionStack ) - 1;
        $this->messagePtr   = 0;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function send( /* string */ $message )
    {
        $this->broker->send( $message );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function recv()
    {
        if ( $this->messagePtr < count($this->actionStack[ $this->actionPtr ]) )
            return $this->actionStack[ $this->actionPtr ][ $this->messagePtr++ ];

        if ( $this->actionPtr > 0 )
            throw new Exception\UnderflowException();

        throw new Exception\ContinueException();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional( $func )
    {
        if ( $this->messagePtr < count($this->actionStack[ $this->actionPtr ]) )
            throw new Exception\OverflowException();

        if ( $this->actionPtr > 0 )
            $this->actionPtr --;
        else
            array_unshift( $this->actionStack, [] );

        $this->messagePtr = 0;
        $val = $func( $this );

        if ( $this->actionPtr > 0 ||
             $this->messagePtr < count($this->actionStack[ $this->actionPtr ]) )
            throw new Exception\OverflowException();

        array_shift( $this->actionStack );
        $this->messagePtr = count($this->actionStack[ $this->actionPtr ]);

        return $val;
    }

    /**
     * append message to stack
     *
     * @param string $message
     * @return $this
     */
    public function append( /* string */ $message )
    {
        array_push( $this->actionStack[0], $message );

        return $this;
    }

    /**
     * freeze action stack
     *
     * @return string[][]
     */
    public function freeze()
    {
        return $this->actionStack;
    }
}
