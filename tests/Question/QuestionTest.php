<?php

namespace AzureSpring\DeterministicDialogue\Tests\Question;

use AzureSpring\DeterministicDialogue\Question\Question;
use PHPUnit\Framework\TestCase;

/**
 * @covers AzureSpring\DeterministicDialogue\Question\Question
 */
class QuestionTest extends TestCase
{
    public function testConstruct()
    {
        $this->assertInstanceOf(
            Question::class,
            new Question( 'A question' ));
    }

    public function testGetQuestion()
    {
        $this->assertEquals(
            'A question',
            (new Question( 'A question' ))->getQuestion());
    }

    public function testFilter()
    {
        $this->assertEquals(
            'The answer',
            (new Question( 'A question' ))->filter('The answer'));
    }
}
