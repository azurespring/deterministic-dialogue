<?php

namespace AzureSpring\DeterministicDialogue\Tests\Question;

use AzureSpring\DeterministicDialogue\Question\ChoiceQuestion;
use PHPUnit\Framework\TestCase;

/**
 * @covers AzureSpring\DeterministicDialogue\Question\ChoiceQuestion
 */
class ChoiceQuestionTest extends TestCase
{
    public function testConstruct()
    {
        $this->assertInstanceOf(
            ChoiceQuestion::class,
            new ChoiceQuestion( [] ));
    }

    public function testGetQuestionShouldNumberOptions()
    {
        $this->assertEquals(
            <<< __QUESTION__
1. Option A
2. Option B
3. Option C
__QUESTION__
            ,
            (new ChoiceQuestion([
                'Option A',
                'Option B',
                'Option C',
            ]))->getQuestion());
    }

    public function testFilterShouldAcceptNumberKey()
    {
        $this->assertEquals(
            2,
            (new ChoiceQuestion([
                'Option A',
                'Option B',
                'Option C',
            ]))->filter( "2" ));
    }

    public function testFilterShouldNotAcceptOutOfRangeNumberKey()
    {
        $this->assertFalse(
            (new ChoiceQuestion([
                'Option A',
                'Option B',
                'Option C',
            ]))->filter( "5" ));
    }

    public function testGetQuestionShouldNotNumberStringKey()
    {
        $this->assertEquals(
            <<< __QUESTION__
a. Avant
1. Option A
b. Blues
c. Country
__QUESTION__
            ,
            (new ChoiceQuestion([
                'a' => 'Avant',
                'Option A',
                'b' => 'Blues',
                'c' => 'Country',
            ]))->getQuestion());
    }

    public function testFilterShouldAcceptStringKey()
    {
        $this->assertEquals(
            'b',
            (new ChoiceQuestion([
                'a' => 'Avant',
                'Option A',
                'b' => 'Blues',
                'c' => 'Country',
            ]))->filter( 'b' ));
    }

    public function testFilterShouldNotAcceptNonExistStringKey()
    {
        $this->assertFalse(
            (new ChoiceQuestion([
                'a' => 'Avant',
                'Option A',
                'b' => 'Blues',
                'c' => 'Country',
            ]))->filter( 'e' ));
    }

    public function testGetQuestionShouldNotLabelNonWordKey()
    {
        $this->assertEquals(
            <<< __QUESTION__
A legend
a. Avant
Another legend
1. Option A
Yet another legend
b. Blues
c. Country
__QUESTION__
            ,
            (new ChoiceQuestion([
                '+' => 'A legend',
                'a' => 'Avant',
                '*' => 'Another legend',
                'Option A',
                '#' => 'Yet another legend',
                'b' => 'Blues',
                'c' => 'Country',
            ]))->getQuestion());
    }

    public function testFilterShouldNotAcceptNonWordKey()
    {
        $this->assertFalse(
            (new ChoiceQuestion([
                '+' => 'A legend',
                'a' => 'Avant',
                '*' => 'Another legend',
                'Option A',
                '#' => 'Yet another legend',
                'b' => 'Blues',
                'c' => 'Country',
            ]))->filter( '*' ));
    }
}
