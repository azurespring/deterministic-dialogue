<?php

namespace AzureSpring\DeterministicDialogue\Tests;

use AzureSpring\DeterministicDialogue\AbstractDialogue;
use AzureSpring\DeterministicDialogue\Question\QuestionInterface;
use AzureSpring\DeterministicDialogue\Exception\ContinueException;
use PHPUnit\Framework\TestCase;

/**
 * @covers AzureSpring\DeterministicDialogue\AbstractDialogue
 */
class AbstractDialogueTest extends TestCase
{
    public function testAskShouldRecvAnswerIfAvailable()
    {
        $question = $this->createMock( QuestionInterface::class );
        $question
            ->expects( $this->atLeastOnce() )
            ->method( 'filter' )
            ->with( $this->equalTo('aha') )
            ->willReturn( 'aha' );

        $dialogue = $this->getMockForAbstractClass( AbstractDialogue::class );
        $dialogue
            ->expects( $this->atLeastOnce() )
            ->method( 'recv' )
            ->willReturn( 'aha' );

        $this->assertEquals(
            'aha',
            $dialogue->ask( $question ));
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\ContinueException
     */
    public function testAskShouldSendQuestionIfNotAvailable()
    {
        $question = $this->createMock( QuestionInterface::class );
        $question
            ->expects( $this->atLeastOnce() )
            ->method( 'getQuestion' )
            ->willReturn( 'A question' );
        $question
            ->expects( $this->never() )
            ->method( 'filter' );

        $dialogue = $this->getMockForAbstractClass( AbstractDialogue::class );
        $dialogue
            ->expects( $this->atLeastOnce() )
            ->method( 'recv' )
            ->will( $this->throwException(new ContinueException()) );
        $dialogue
            ->expects( $this->once() )
            ->method( 'send' )
            ->with( 'A question' );

        $dialogue->ask( $question );
    }
}
