<?php

namespace AzureSpring\DeterministicDialogue\Tests;

use AzureSpring\DeterministicDialogue\ContinuumDialogue;
use AzureSpring\DeterministicDialogue\BrokerInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers AzureSpring\DeterministicDialogue\ContinuumDialogue
 */
class ContinuumDialogueTest extends TestCase
{
    protected $broker;


    public function setUp()
    {
        $this->broker = $this->createMock( BrokerInterface::class );
    }

    public function testConstruct()
    {
        $this->assertInstanceOf(
            ContinuumDialogue::class,
            new ContinuumDialogue( $this->broker ));
    }

    public function testFreezeShouldGetActionStack()
    {
        $this->assertEquals(
            [[]],
            (new ContinuumDialogue( $this->broker ))
            ->freeze());
        $this->assertEquals(
            [[ 'oops' ],
             [ 'hello', 'world' ],
            ],
            (new ContinuumDialogue(
                $this->broker,
                [[ 'oops' ],
                 [ 'hello', 'world' ]
                ]
            ))
            ->freeze());
    }

    public function testAppend()
    {
        $this->assertEquals(
            [[ 'oops', 'aha' ],
             [ 'hello', 'world' ],
            ],
            (new ContinuumDialogue(
                $this->broker,
                [[ 'oops' ],
                 [ 'hello', 'world' ]
                ]
            ))
            ->append( 'aha' )
            ->freeze());
    }

    public function testReset()
    {
        $this->assertEquals(
            [[]],
            (new ContinuumDialogue(
                $this->broker,
                [[ 'oops' ],
                 [ 'hello', 'world' ]
                ]
            ))
            ->reset()
            ->freeze());
    }

    public function testSend()
    {
        $this->broker
            ->expects( $this->once() )
            ->method( 'send' )
            ->with( $this->equalTo('hi') );
        (new ContinuumDialogue( $this->broker ))
            ->send( 'hi' );
    }

    public function testRecvShouldRebuildMessageQueue()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'hello', 'world' ]]
        );

        $this->assertEquals( 'hello', $dialogue->recv() );
        $this->assertEquals( 'world', $dialogue->recv() );
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\ContinueException
     */
    public function testRecvShouldThrowContinueExceptionOnStackTop()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'hello', 'world' ]]
        );

        $dialogue->recv();
        $dialogue->recv();

        $dialogue->recv();
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\UnderflowException
     */
    public function testRecvShouldThrowUnderflowExceptionNotOnStackTop()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'oops' ],
             [ 'hello', 'world' ]
            ]
        );

        $dialogue->recv();
        $dialogue->recv();

        $dialogue->recv();
    }

    public function testTransactionalShouldEnumerateActionStack()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'oops' ],
             [ 'hello', 'world' ]
            ]
        );

        $this->assertEquals( 'hello', $dialogue->recv() );
        $this->assertEquals( 'world', $dialogue->recv() );

        $dialogue->transactional(function ( ContinuumDialogue $dialogue ) {
            $this->assertEquals( 'oops', $dialogue->recv() );
        });
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\ContinueException
     */
    public function testTransactionalShouldCreateNewActionIfNecessary()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'hello', 'world' ]
            ]
        );

        $dialogue->recv();
        $dialogue->recv();

        $dialogue->transactional(function ( ContinuumDialogue $dialogue ) {
            $this->assertEquals(
                [[],
                 [ 'hello', 'world' ]
                ],
                $dialogue->recv()->freeze());
        });
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\OverflowException
     */
    public function testTransactionalShouldDrainOutActionOnBegin()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'oops' ],
             [ 'hello', 'world' ]
            ]
        );

        $dialogue->recv();

        $dialogue->transactional(function ( ContinuumDialogue $dialogue ) {
            $dialogue->recv();
        });
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\OverflowException
     */
    public function testTransactionalShouldDrainOutStackTopOnEnd()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'oops', 'aha' ],
             [ 'hello', 'world' ]
            ]
        );

        $dialogue->recv();
        $dialogue->recv();

        $dialogue->transactional(function ( ContinuumDialogue $dialogue ) {
            $dialogue->recv();
        });
    }

    /**
     * @expectedException AzureSpring\DeterministicDialogue\Exception\OverflowException
     */
    public function testTransactionalShouldOnlyDrainOutStackTopOnEnd()
    {
        $dialogue = new ContinuumDialogue(
            $this->broker,
            [[ 'aha' ],
             [ 'oops' ],
             [ 'hello', 'world' ]
            ]
        );

        $dialogue->recv();
        $dialogue->recv();

        $dialogue->transactional(function ( ContinuumDialogue $dialogue ) {
            $dialogue->recv();
        });
    }
}
